/*
Реалізувати просту програму на Javascript, яка взаємодіятиме з користувачем
за допомогою модальних вікон браузера - alert, prompt, confirm.
Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:

-Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.
-Якщо вік менше 18 років - показати на екрані повідомлення: You are not allowed to visit this website.
-Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням: Are you sure you want to continue?
    і кнопками Ok, Cancel. Якщо користувач натиснув Ok, показати на екрані повідомлення: Welcome,  + ім'я користувача.
    Якщо користувач натиснув Cancel, показати на екрані повідомлення: You are not allowed to visit this website.
-Якщо вік більше 22 років – показати на екрані повідомлення: Welcome,  + ім'я користувача.
-Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
-Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів ім'я,
    або при введенні віку вказав не число - запитати ім'я та вік наново
    (при цьому дефолтним значенням для кожної зі змінних має бути введена раніше інформація).
*/
"use strict";
const getUserNameAndAge = function () {
  let userName = "";
  let userAge = "";
  const userNameMessage = "Please enter Your name";
  const userAgeMessage = "Please enter Your age (any number)";
  do {
    userName = prompt(userNameMessage, userName);
    if (userName === null) return null;
    userAge = prompt(userAgeMessage, userAge);
    if (userAge === null) return null;
  } while (!userName || !Number(userAge));
  return [userName, +userAge];
};
function showAccessLevel(userName, userAge) {
  let accessAllowed = false;
  const accessConfirmMessage = "Are you sure you want to continue?";
  const accessAllowedMessage = `Welcome,  ${userName}`;
  const accessNotAllowedMessage = "You are not allowed to visit this website";
  if (userAge > 22) {
    accessAllowed = true;
  } else if (userAge >= 18 && userAge <= 22) {
    accessAllowed = confirm(accessConfirmMessage) ? true : false;
  }
  alert(accessAllowed ? accessAllowedMessage : accessNotAllowedMessage);
}

const userNameAndAge = getUserNameAndAge();
if (userNameAndAge !== null) {
  showAccessLevel(...userNameAndAge);
}
